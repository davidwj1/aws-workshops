#!/bin/bash
set -e
export AWS_PROFILE=j1-sandbox-developer

disableDistribution() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"
    local ETAG="$2"
    local DISTRIBUTION_CONFIG_FILE=$(mktemp /tmp/distribution-config.XXXXXX.json)

    aws cloudfront get-distribution-config --id ${CLOUDFRONT_DISTRIBUTION_ID} --query DistributionConfig | jq .Enabled=false >${DISTRIBUTION_CONFIG_FILE}
    aws cloudfront update-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --if-match ${ETAG} --distribution-config file://${DISTRIBUTION_CONFIG_FILE} >/dev/null
    echo -e "Checking if Cloudfront distribution has been deployed"
    aws cloudfront wait distribution-deployed --id ${CLOUDFRONT_DISTRIBUTION_ID}
    rm ${DISTRIBUTION_CONFIG_FILE}
}

getETag() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"

    aws cloudfront get-distribution-config --id ${CLOUDFRONT_DISTRIBUTION_ID} --query ETag --output text
}

deleteDistribution() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"
    local ETAG="$2"

    aws cloudfront delete-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --if-match ${ETAG}
}

destroy() {
    local BUCKET_NAME="$1"
    local CLOUDFRONT_DISTRIBUTION_ID="$2"

    echo -e "Deleting S3 Bucket"
    aws s3 rb --force s3://${BUCKET_NAME}

    echo -e "Removing Cloudfront Distributuion"
    local ETAG=$(getETag ${CLOUDFRONT_DISTRIBUTION_ID})
    disableDistribution ${CLOUDFRONT_DISTRIBUTION_ID} ${ETAG}
    local ETAG=$(getETag ${CLOUDFRONT_DISTRIBUTION_ID})
    deleteDistribution ${CLOUDFRONT_DISTRIBUTION_ID} ${ETAG}
}

DISTRIBUTION_ID=$(aws cloudfront list-distributions --query DistributionList.Items[0].Id --output text)

BUCKET_NAME=$(aws s3api list-buckets --query Buckets[0].Name --output text)

destroy "${BUCKET_NAME}" "${DISTRIBUTION_ID}"