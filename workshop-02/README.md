# AWS Workshop 02 Homework

## Scripts

- deploy.sh is a script to deploy App/build directory into S3 bucket with Cloudfront.
- Call as './deploy.sh "/path/to/app" '

- destroy.sh is a script to destroy the S3 bucket that was created previously as well as the Cloudfront distribution.
- Call as "./destroy.sh"
