#!/bin/bash
set -e
export AWS_PROFILE=j1-sandbox-developer

setBucketPolicy() {
    local BUCKET_NAME=$1
    local BUCKET_POLICY_FILE=$(mktemp /tmp/bucket-policy.XXXXXX.json)
    cat <<EOF >${BUCKET_POLICY_FILE}
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::${BUCKET_NAME}/*"
            ]
        }
    ]
}
EOF
    aws s3api put-bucket-policy --bucket ${BUCKET_NAME} --policy file://${BUCKET_POLICY_FILE}
    rm ${BUCKET_POLICY_FILE}
}

deploy() {
    local WEBSITE_CONTENTS="$1"
    local BUCKET_NAME=$(uuidgen)

    # deploy s3 bucket
    aws s3 mb s3://${BUCKET_NAME}

    aws s3 website s3://${BUCKET_NAME} --index-document index.html

    aws s3api delete-public-access-block --bucket ${BUCKET_NAME}

    aws s3 sync "${WEBSITE_CONTENTS}" s3://${BUCKET_NAME}

    setBucketPolicy ${BUCKET_NAME}

    local S3_HTTP_DOMAIN_NAME="${BUCKET_NAME}.s3-website.ap-southeast-2.amazonaws.com"

    local DOMAIN_NAME=$(aws cloudfront create-distribution --origin-domain-name ${S3_HTTP_DOMAIN_NAME} --default-root-object index.html | jq '.Distribution.DomainName' -r)
    local CLOUDFRONT_DISTRIBUTION_ID=$(aws cloudfront list-distributions --query DistributionList.Items[0].Id --output text)
    echo -e "Checking if Cloudfront distribution has been deployed"
    aws cloudfront wait distribution-deployed --id ${CLOUDFRONT_DISTRIBUTION_ID}
    # return endpoint
    echo ${DOMAIN_NAME}
}

# ./deploy2.sh "awsWorkshop/bit-by-bit/build"
deploy ~/"$1"