#!/bin/bash

# How to Run
# ./deploy <absolute path to build folder> <aws sso profile>

set -e -x
export AWS_PROFILE=j1-sandbox-developer

createDistributionConfig() {
    local BUCKET_NAME="$1"
    local OAI_ID="$2"
    local CACHE_POLICY_ID="$3"
    local DISTRIBUTION_CONFIG_FILE=$(mktemp /tmp/distribution-config.XXXXXX.json)

    cat <<EOF >${DISTRIBUTION_CONFIG_FILE}
{
  "DefaultRootObject": "index.html",
  "CallerReference": "${BUCKET_NAME}",
  "Comment": "Serves content for ${BUCKET_NAME}",
  "Enabled": true,
  "Origins": {
    "Quantity": 1,
    "Items": [
      {
        "Id": "${BUCKET_NAME}",
        "DomainName": "${BUCKET_NAME}.s3.ap-southeast-2.amazonaws.com",
        "S3OriginConfig": {
          "OriginAccessIdentity": "origin-access-identity/cloudfront/${OAI_ID}"
        }
      }
    ]
  },
  "DefaultCacheBehavior": {
    "TargetOriginId": "${BUCKET_NAME}",
    "ViewerProtocolPolicy": "redirect-to-https",
    "CachePolicyId": "${CACHE_POLICY_ID}"
  }
}
EOF
    echo "${DISTRIBUTION_CONFIG_FILE}"
}

createCachePolicyConfigFile() {
    local BUCKET_NAME="$1"
    local CACHE_POLICY_CONFIG_FILE=$(mktemp /tmp/cache-policy-config.XXXXXX.json)

    cat <<EOF >${CACHE_POLICY_CONFIG_FILE}
{
  "Comment": "Set cache policy for ${BUCKET_NAME}",
  "Name": "${BUCKET_NAME}",
  "MinTTL": 86400
}
EOF

    echo "${CACHE_POLICY_CONFIG_FILE}"
}

createCachePolicy() {
    local CACHE_CONFIG_FILENAME="$1"
    aws cloudfront create-cache-policy --cache-policy-config file://"${CACHE_CONFIG_FILENAME}" --query CachePolicy.Id --output text
}

setBucketPolicy() {
    local BUCKET_NAME=$1
    local OAI_ID=$2
    local BUCKET_POLICY_FILE=$(mktemp /tmp/bucket-policy.XXXXXX.json)
    cat <<EOF >${BUCKET_POLICY_FILE}
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ${OAI_ID}"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${BUCKET_NAME}/*"
        }
    ]
}
EOF
    aws s3api put-bucket-policy --bucket ${BUCKET_NAME} --policy file://${BUCKET_POLICY_FILE}
    rm ${BUCKET_POLICY_FILE}
}

createOriginAccessIdentity() {
    local BUCKET_NAME="$1"

    # create the origin access identity
    aws cloudfront create-cloud-front-origin-access-identity --cloud-front-origin-access-identity-config CallerReference="${BUCKET_NAME}",Comment="Bucket Origin Identity" --query CloudFrontOriginAccessIdentity.Id --output text
}

deploy() {
    local WEBSITE_CONTENTS="$1"
    local BUCKET_NAME=$(uuidgen)
    local S3_HTTP_DOMAIN_NAME="${BUCKET_NAME}.s3-website.ap-southeast-2.amazonaws.com"

    aws s3 mb s3://${BUCKET_NAME}

    aws s3 sync "${WEBSITE_CONTENTS}" s3://${BUCKET_NAME}

    local OAI_ID=$(createOriginAccessIdentity ${BUCKET_NAME})
    sleep 10

    setBucketPolicy "${BUCKET_NAME}" "${OAI_ID}"

    local CACHE_POLICY_CONFIG_FILENAME=$(createCachePolicyConfigFile "${BUCKET_NAME}")
    local CACHE_POLICY_ID=$(createCachePolicy "${CACHE_POLICY_CONFIG_FILENAME}")
    echo "Cache policy ID ${CACHE_POLICY_ID}"
    local DISTRIBUTION_CONFIG_FILE_NAME=$(createDistributionConfig "${BUCKET_NAME}" "${OAI_ID}" "${CACHE_POLICY_ID}")

    rm ${CACHE_POLICY_CONFIG_FILENAME}

    local CLOUDFRONT_DISTRIBUTION_ID=$(aws cloudfront create-distribution --distribution-config file://"${DISTRIBUTION_CONFIG_FILE_NAME}" --query Distribution.Id --output text)

    rm "${DISTRIBUTION_CONFIG_FILE_NAME}"
    aws cloudfront wait distribution-deployed --id ${CLOUDFRONT_DISTRIBUTION_ID}

    local DOMAIN_NAME=$(aws cloudfront get-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --query Distribution.DomainName --output text)

    echo ${DOMAIN_NAME}
}

# ./deploy-V2.sh "awsWorkshop/bit-by-bit/build"
deploy ~/"$1"